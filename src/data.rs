use serde::{Serialize, Deserialize};

pub fn get_data() -> HourlyForecast {
    let example = HourlyForecast {
        wind_direction: "W".to_string(),
        wind_speed: 4,
        speed_units: "mph".to_string(),
        temperature: 25,
        temperature_unit: "F".to_string(),
        summary: "Cloudy with a chance of meatballs".to_string()
    };
    return example
}

#[derive(Serialize, Deserialize)]
#[derive(Debug)]
pub struct HourlyForecast {
    wind_direction: String,
    wind_speed: u8,
    speed_units: String,
    temperature: u8,
    temperature_unit: String,
    summary: String,
}
