use wasm_bindgen::prelude::*;

mod data;

#[wasm_bindgen]
pub fn get_data_js(name: &str) -> JsValue {
    let example = data::get_data();
   JsValue::from_serde(&example).unwrap()
}
